#################################################################
#                                                               #
# Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
import yottadb

from typing import AnyStr
from flask import Blueprint, render_template, request, redirect

from YDBApron.globals import GRAMS_PER_OUNCE, OUNCES_PER_POUND, GRAMS_PER_MILLILITER, MAX_RECIPES
from YDBApron.forms import RecipeForm, ScheduleForm
from YDBApron.ingredients import get_ingredients

blueprint = Blueprint("recipes", __name__, url_prefix="/recipes")


def total_recipes() -> int:
    total = 0
    for category in yottadb.subscripts("^YDBApron", ("recipes", "")):
        for recipe in yottadb.subscripts("^YDBApron", ("recipes", category, "")):
            total += 1

    print(f"Total recipes: {total}")
    return total


def total_in_grams(amount: float, unit: AnyStr) -> float:
    if "g" == unit:
        return amount
    elif "oz" == unit:
        return amount * GRAMS_PER_OUNCE
    elif "lb" == unit:
        return amount * GRAMS_PER_OUNCE * OUNCES_PER_POUND
    elif "ml" == unit:
        return amount * GRAMS_PER_MILLILITER
    else:
        assert(False)
        return amount


def add_recipe(recipe_form: RecipeForm):
    ydbapron = yottadb.Key("^YDBApron")
    recipe_name = recipe_form.recipe_name.data
    recipe_category = recipe_form.recipe_category.data
    gross_yield = 0
    # Get the gross yield first in order calculate ingredient proportions below
    for ingredient in recipe_form.ingredients:
        if ingredient.ingredient_name.data and ingredient.ingredient_category.data and ingredient.ingredient_amount.data:
            gross_yield += float(ingredient.ingredient_amount.data)
    for ingredient in recipe_form.ingredients:
        ingredient_name = ingredient.ingredient_name.data
        ingredient_category = ingredient.ingredient_category.data
        ingredient_amount = ingredient.ingredient_amount.data
        ingredient_unit = ingredient.ingredient_unit.data
        # TODO: Add proper validation (and dynamic ingredient additions) client-side so that it's not necessary to implicitly ignore
        # empty fields here. Then, this check can also be removed.
        if ingredient_amount and ingredient_unit:
            # Only add fully defined ingredients to the database
            ingredient_proportion = float(ingredient.ingredient_amount.data) / gross_yield
            ydbapron["recipes"][recipe_category][recipe_name]["ingredients"][ingredient_category][ingredient_name].value = f"{ingredient_amount}|{ingredient_unit}|{ingredient_proportion}"
        else:
            print("WARNING: Incomplete ingredient information received. Omitting entry from database.")

    for specification in recipe_form.specifications:
        specification_format = specification.specification_format.data
        specification_format_size = specification.specification_format_size.data
        specification_size = specification.specification_size.data
        specification_unit = specification.specification_unit.data
        if specification_format and specification_format_size and specification_size and specification_unit:
            # Only add fully defined product specifications to the database
            ydbapron["recipes"][recipe_category][recipe_name]["specifications"][specification_format][specification_format_size].value = f"{specification_size}|{specification_unit}"
        else:
            print("WARNING: Incomplete specification information received. Omitting entry from database.")
    ydbapron["recipes"][recipe_category][recipe_name]["procedure"].value = recipe_form.procedure.data.encode("utf-8")

    return


def delete_recipe(category: AnyStr, name: AnyStr):
    yottadb.delete_tree("^YDBApron", ("recipes", category, name))

    return


def list_recipes() -> dict:
    recipes = {}
    for category in yottadb.subscripts("^YDBApron", ("recipes", "")):
        category = category.decode("utf-8")
        recipes[category] = []
        for recipe in yottadb.subscripts("^YDBApron", ("recipes", category, "")):
            recipes[category].append(recipe.decode("utf-8"))

    return recipes


# Retrieve stored data for the given recipe from the database and
# return as a dict.
def get_recipe(category: AnyStr, recipe_name: AnyStr) -> dict:
    ydbapron = yottadb.Key("^YDBApron")
    recipe = {}
    recipe["name"] = recipe_name
    recipe["category"] = category
    recipe["ingredients"] = {}
    recipe["gross_cost"] = 0
    recipe["yield"] = 0
    # Retrieve recipe ingredient information
    for ingredient_category in ydbapron["recipes"][category][recipe_name]["ingredients"].subscripts:
        ingredient_category = ingredient_category.decode("utf-8")
        recipe["ingredients"][ingredient_category] = {}
        for ingredient in ydbapron["recipes"][category][recipe_name]["ingredients"][ingredient_category].subscripts:
            # Get amount of each ingredient used in the recipe
            ingredient_unit = ydbapron["recipes"][category][recipe_name]["ingredients"][ingredient_category][ingredient].value.decode("utf-8")
            ingredient = ingredient.decode("utf-8")
            recipe["ingredients"][ingredient_category][ingredient] = {}
            recipe["ingredients"][ingredient_category][ingredient]["amount"] = float(ingredient_unit.split("|")[0])
            recipe["ingredients"][ingredient_category][ingredient]["unit"] = ingredient_unit.split("|")[1]
            recipe["ingredients"][ingredient_category][ingredient]["proportion"] = float(ingredient_unit.split("|")[2])
            recipe["yield"] += total_in_grams(recipe["ingredients"][ingredient_category][ingredient]["amount"], recipe["ingredients"][ingredient_category][ingredient]["unit"])
            # Cross-reference the ingredient(s) used in the recipe with the specifications
            # recorded for the given ingredient to calculate recipe costs.
            ingredient_spec = ydbapron["ingredients"][ingredient_category][ingredient].value.decode("utf-8")
            # Ensure ingredient specification and recipe use same unit measure
            assert(recipe["ingredients"][ingredient_category][ingredient]["unit"] == ingredient_spec.split("|")[1])
            # Calculate cost of each unit of the given ingredient, e.g. $/g
            cost_per_unit = float(ingredient_spec.split("|")[2]) / float(ingredient_spec.split("|")[0])
            # Calculate total cost of the given ingredient in the given recipe
            recipe["ingredients"][ingredient_category][ingredient]["cost"] = cost_per_unit * recipe["ingredients"][ingredient_category][ingredient]["amount"]
            recipe["gross_cost"] += recipe["ingredients"][ingredient_category][ingredient]["cost"]
    recipe["unit_cost"] = recipe["gross_cost"] / recipe["yield"]

    # Retrieve product specification, e.g. the "German Rye" comes in a loaf format, each weighing 800 grams.
    recipe["specifications"] = {}
    for product_format in ydbapron["recipes"][category][recipe_name]["specifications"].subscripts:
        product_format = product_format.decode("utf-8")
        recipe["specifications"][product_format] = {}
        for product_size in ydbapron["recipes"][category][recipe_name]["specifications"][product_format].subscripts:
            product_spec = ydbapron["recipes"][category][recipe_name]["specifications"][product_format][product_size].value.decode("utf-8")
            product_size = product_size.decode("utf-8")
            recipe["specifications"][product_format][product_size] = {}
            recipe["specifications"][product_format][product_size]["amount"] = float(product_spec.split("|")[0])
            recipe["specifications"][product_format][product_size]["unit"] = product_spec.split("|")[1]
            recipe["specifications"][product_format][product_size]["cost"] = recipe["unit_cost"] * recipe["specifications"][product_format][product_size]["amount"]

    recipe["schedules"] = []
    for schedule in ydbapron["schedules"].subscripts:
        if 0 < ydbapron["schedules"][schedule][category][recipe_name].data:
            recipe["schedules"].append(schedule.decode("utf-8"))
    recipe["procedure"] = ydbapron["recipes"][category][recipe_name]["procedure"].value.decode("utf-8")

    return recipe


def get_recipes(form: ScheduleForm) -> dict:
    ydbapron = yottadb.Key("^YDBApron")
    recipes = {}
    recipe_forms = iter(form.recipes)
    for category in ydbapron["recipes"].subscripts:
        category = category.decode("utf-8")
        recipes[category] = {}
        for recipe in ydbapron["recipes"][category].subscripts:
            recipe = recipe.decode("utf-8")
            recipes[category][recipe] = get_recipe(category, recipe)
            try:
                recipe_form = next(recipe_forms)
            except StopIteration:
                break
            recipes[category][recipe]["form"] = recipe_form
            specification_forms = iter(recipe_form.specifications)
            for product_format in recipes[category][recipe]["specifications"]:
                for product_size in recipes[category][recipe]["specifications"][product_format]:
                    try:
                        recipes[category][recipe]["specifications"][product_format][product_size]["form"] = next(specification_forms)
                    except StopIteration:
                        break

    return recipes


@blueprint.route("/list", methods=("GET", "POST"))
def list():
    recipes = list_recipes()

    return render_template('recipes.html', recipes=recipes)


@blueprint.route("/<category>/<recipe_name>", methods=("GET", "POST"))
def recipe(category: AnyStr, recipe_name: AnyStr):
    # Replace spaces with dashes to construct a valid URL, which cannot contain spaces
    recipe = get_recipe(category, recipe_name.replace("-", " "))

    return render_template('recipe.html', recipe=recipe)


@blueprint.route("/add", methods=("GET", "POST"))
def add():

    form = RecipeForm(request.form)
    num_recipes = total_recipes()
    entries_available = num_recipes < MAX_RECIPES
    if request.method == "POST":
        if entries_available:
            add_recipe(form)
            # Redirect browser to the recipe page for the newly added recipe. The URL directs to the recipe() route.
            # Also, replace spaces with dashes to construct a valid URL, which cannot contain spaces
            return redirect(f"/recipes/{form.recipe_category.data.replace(' ', '-')}/{form.recipe_name.data.replace(' ', '-')}")
        else:
            return render_template('add_recipe.html', recipe_form=form, ingredients=get_ingredients(form), operation="Add", entries_available=entries_available, num_recipes=num_recipes)
    else:
        form.procedure.data = ""
        return render_template('add_recipe.html', recipe_form=form, ingredients=get_ingredients(form), operation="Add", entries_available=entries_available, num_recipes=num_recipes)


@blueprint.route("/edit/<category>/<recipe_name>", methods=("GET", "POST"))
def edit(category: AnyStr, recipe_name: AnyStr):
    form = RecipeForm(request.form)
    if request.method == "POST":
        delete_recipe(category, recipe_name)
        add_recipe(form)
        return redirect(f"/recipes/{category}/{recipe_name}")
    else:
        # Replace spaces with dashes to construct a valid URL, which cannot contain spaces
        recipe = get_recipe(category, recipe_name.replace("-", " "))
        ingredients = get_ingredients(form)
        # Pre-populate form with recipe information from database
        form.recipe_name.data = recipe["name"]
        form.recipe_category.data = recipe["category"]
        form.procedure.data = recipe["procedure"] if recipe["procedure"] is not None else ""
        # Pre-populate ingredient information
        for ingredient_category in recipe["ingredients"]:
            for ingredient in recipe["ingredients"][ingredient_category]:
                ingredients[ingredient_category][ingredient]["form"].ingredient_amount.data = recipe["ingredients"][ingredient_category][ingredient]["amount"]
                ingredients[ingredient_category][ingredient]["form"].ingredient_unit.data = recipe["ingredients"][ingredient_category][ingredient]["unit"]
        # Pre-populate product specification information
        specification_forms = iter(form.specifications)
        for product_format in recipe["specifications"]:
            for product_size in recipe["specifications"][product_format]:
                specification_form = next(specification_forms)
                specification_form.specification_format.data = product_format
                specification_form.specification_format_size.data = product_size
                specification_form.specification_size.data = recipe["specifications"][product_format][product_size]["amount"]
                specification_form.specification_unit.data = recipe["specifications"][product_format][product_size]["unit"]
        return render_template('add_recipe.html', recipe_form=form, ingredients=ingredients, operation="Edit", entries_available=True)


@blueprint.route("/delete", methods=("POST",))
def delete():
    form = request.form
    delete_recipe(form["deleteCategory"], form["deleteRecipe"])

    return redirect("/recipes/list")
