#################################################################
#                                                               #
# Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
import yottadb

from typing import AnyStr

from YDBApron.recipes import get_recipe, get_recipes
from YDBApron.forms import ScheduleForm

from flask import Blueprint, render_template, request, redirect

blueprint = Blueprint("schedules", __name__, url_prefix="/schedules")


def list_schedules() -> list:
    schedules = []
    for schedule in yottadb.subscripts("^YDBApron", ("schedules", "")):
        schedules.append(schedule.decode("utf-8"))

    return schedules


def get_schedule(schedule_name: AnyStr) -> dict:
    ydbapron = yottadb.Key("^YDBApron")
    schedule = {}
    schedule["name"] = schedule_name
    schedule["recipes"] = {}
    for category in ydbapron["schedules"][schedule_name].subscripts:
        category = category.decode("utf-8")
        schedule["recipes"][category] = {}
        for recipe in ydbapron["schedules"][schedule_name][category].subscripts:
            recipe = recipe.decode("utf-8")
            schedule["recipes"][category][recipe] = get_recipe(category, recipe)
            schedule["recipes"][category][recipe]["gross_yield"] = 0
            for specification in ydbapron["schedules"][schedule_name][category][recipe]["specifications"].subscripts:
                specification = specification.decode("utf-8")
                schedule["recipes"][category][recipe][specification] = {}
                for size in ydbapron["schedules"][schedule_name][category][recipe]["specifications"][specification].subscripts:
                    size = size.decode("utf-8")
                    product_volume = float(ydbapron["schedules"][schedule_name][category][recipe]["specifications"][specification][size].value.decode("utf-8"))
                    schedule["recipes"][category][recipe][specification][size] = product_volume
                    product_size = float(ydbapron["recipes"][category][recipe]["specifications"][specification][size].value.decode("utf-8").split("|")[0])
                    schedule["recipes"][category][recipe]["gross_yield"] += product_volume * product_size
            schedule["recipes"][category][recipe]["supplementary_yield"] = float(ydbapron["schedules"][schedule_name][category][recipe]["supplementary"].value)
            schedule["recipes"][category][recipe]["gross_yield"] += schedule["recipes"][category][recipe]["supplementary_yield"]

            cost = schedule["recipes"][category][recipe]["unit_cost"]
            schedule["recipes"][category][recipe]["cost"] = cost
            schedule["recipes"][category][recipe]["cost_unit"] = "g"
            schedule["recipes"][category][recipe]["gross_cost"] = cost * schedule["recipes"][category][recipe]["gross_yield"]

            for ingredient_category in schedule["recipes"][category][recipe]["ingredients"]:
                for ingredient in schedule["recipes"][category][recipe]["ingredients"][ingredient_category]:
                    schedule["recipes"][category][recipe]["ingredients"][ingredient_category][ingredient]["amount"] = schedule["recipes"][category][recipe]["ingredients"][ingredient_category][ingredient]["proportion"] * schedule["recipes"][category][recipe]["gross_yield"]
                    schedule["recipes"][category][recipe]["ingredients"][ingredient_category][ingredient]["cost"] = schedule["recipes"][category][recipe]["ingredients"][ingredient_category][ingredient]["amount"] * cost
    return schedule


def add_schedule(schedule_form: ScheduleForm):
    ydbapron = yottadb.Key("^YDBApron")
    schedule_name = schedule_form.schedule_name.data
    for recipe_form in schedule_form.recipes:
        recipe_name = recipe_form.recipe_name.data
        recipe_category = recipe_form.recipe_category.data
        if recipe_name is None or recipe_category is None:
            break
        supplementary_yield = recipe_form.supplementary_yield.data if recipe_form.supplementary_yield.data is not None else 0
        for specification_form in recipe_form.specifications:
            specification_name = specification_form.specification_name.data
            specification_size = specification_form.specification_size.data
            specification_volume = specification_form.specification_volume.data
            # Confirm the form was filled out. Blank forms will be present when the number
            # of recipe specifications in the database is fewer than the maximum allowed.
            if specification_name and specification_size and specification_volume:
                ydbapron["schedules"][schedule_name][recipe_category][recipe_name]["specifications"][specification_name][specification_size].value = str(specification_volume)
            else:
                # No more completed forms are present, so just break here
                # and ignore any remaining blank forms.
                break
        if 0 < supplementary_yield or 0 < ydbapron["schedules"][schedule_name][recipe_category][recipe_name].data:
            # Only add the supplementary yield when it is non-zero, or when at least one recipe specification is present.
            # If there are no recipe specifications and the supplementary yield is 0, then the gross yield is 0.
            # In that case, the recipe can be omitted from the schedule since no product is scheduled to be produced.
            ydbapron["schedules"][schedule_name][recipe_category][recipe_name]["supplementary"].value = str(supplementary_yield)

    return


def delete_schedule(name: AnyStr):
    yottadb.delete_tree("^YDBApron", ("schedules", name))

    return


@blueprint.route("/list", methods=("GET", "POST"))
def list():
    schedules = list_schedules()

    return render_template('schedules.html', schedules=schedules)


@blueprint.route("/<schedule_name>", methods=("GET", "POST"))
def schedule(schedule_name: AnyStr):
    recipe_schedule = get_schedule(schedule_name)

    return render_template('schedule.html', schedule=recipe_schedule)


@blueprint.route("/add", methods=("GET", "POST"))
def add():
    form = ScheduleForm(request.form)
    if request.method == "POST":
        add_schedule(form)
        # Replace spaces with dashes to construct a valid URL, which cannot contain spaces
        return redirect(f"/schedules/{form.schedule_name.data.replace(' ', '-')}")
    else:
        recipes = get_recipes(form)
        return render_template('add_schedule.html', schedule_form=form, recipes=recipes, operation="Add")


@blueprint.route("/edit/<schedule_name>", methods=("GET", "POST"))
def edit(schedule_name: AnyStr):
    form = ScheduleForm(request.form)
    if request.method == "POST":
        delete_schedule(schedule_name)
        add_schedule(form)
        # Replace spaces with dashes to construct a valid URL, which cannot contain spaces
        return redirect(f"/schedules/{form.schedule_name.data.replace(' ', '-')}")
    else:
        schedule = get_schedule(schedule_name)
        recipes = get_recipes(form)
        # Pre-populate form with schedule information from database
        form.schedule_name.data = schedule_name
        # Pre-populate recipe information
        for recipe_category in recipes:
            for recipe in recipes[recipe_category]:
                recipes[recipe_category][recipe]["form"].recipe_name.data = recipe
                recipes[recipe_category][recipe]["form"].recipe_category.data = recipe_category
                recipes[recipe_category][recipe]["form"].supplementary_yield.data = schedule["recipes"][recipe_category][recipe]["supplementary_yield"]
                recipes[recipe_category][recipe]["form"].gross_yield.data = schedule["recipes"][recipe_category][recipe]["gross_yield"]
                recipes[recipe_category][recipe]["form"].gross_cost.data = schedule["recipes"][recipe_category][recipe]["gross_cost"]
                for product_format in recipes[recipe_category][recipe]["specifications"]:
                    for product_size in recipes[recipe_category][recipe]["specifications"][product_format]:
                        recipes[recipe_category][recipe]["specifications"][product_format][product_size]["form"].specification_name.data = product_format
                        recipes[recipe_category][recipe]["specifications"][product_format][product_size]["form"].specification_size.data = product_size
                        try:
                            recipes[recipe_category][recipe]["specifications"][product_format][product_size]["form"].specification_volume.data = schedule["recipes"][recipe_category][recipe][product_format][product_size]
                        except KeyError:
                            recipes[recipe_category][recipe]["specifications"][product_format][product_size]["form"].specification_volume.data = 0

        return render_template('add_schedule.html', schedule_form=form, recipes=recipes, operation="Edit")


@blueprint.route("/delete", methods=("POST",))
def delete():
    form = request.form
    delete_schedule(form["deleteSchedule"])

    return redirect("/schedules/list")
