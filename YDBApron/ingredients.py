#################################################################
#                                                               #
# Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
import yottadb

from typing import AnyStr
from flask import Blueprint, render_template, request, redirect

from YDBApron.forms import RecipeForm, IngredientForm
from YDBApron.globals import MAX_INGREDIENTS

blueprint = Blueprint("ingredients", __name__, url_prefix="/ingredients")


def total_ingredients() -> int:
    total = 0
    for category in yottadb.subscripts("^YDBApron", ("ingredients", "")):
        for ingredient in yottadb.subscripts("^YDBApron", ("ingredients", category, "")):
            total += 1

    print(f"Total ingredients: {total}")
    return total


def save_ingredient(form: IngredientForm):
    ingredient_name = form.ingredient_name.data
    ingredient_category = form.ingredient_category.data
    amount = form.ingredient_amount.data
    unit = form.ingredient_unit.data
    price = form.ingredient_price.data
    manufacturer = form.ingredient_manufacturer.data
    vendor = form.ingredient_vendor.data
    yottadb.set("^YDBApron", ("ingredients", ingredient_category, ingredient_name), f"{amount}|{unit}|{price}|{manufacturer}|{vendor}")


def delete_ingredient(category: AnyStr, ingredient: AnyStr):
    yottadb.delete_node("^YDBApron", ("ingredients", category, ingredient))


def list_ingredients() -> dict:
    ydbapron = yottadb.Key("^YDBApron")
    ingredients = {}
    for category in ydbapron["ingredients"].subscripts:
        category = category.decode("utf-8")
        ingredients[category] = {}
        for ingredient in ydbapron["ingredients"][category].subscripts:
            ingredient = ingredient.decode("utf-8")
            spec = ydbapron["ingredients"][category][ingredient].value.decode("utf-8").split("|")
            ingredients[category][ingredient] = {}
            ingredients[category][ingredient]["amount"] = spec[0]
            ingredients[category][ingredient]["unit"] = spec[1]
            ingredients[category][ingredient]["price"] = spec[2]
            ingredients[category][ingredient]["manufacturer"] = spec[3]
            ingredients[category][ingredient]["vendor"] = spec[4]
            ingredients[category][ingredient]["dependencies"] = []
            for recipe_category in ydbapron["recipes"].subscripts:
                for recipe in ydbapron["recipes"][recipe_category].subscripts:
                    recipe = recipe.decode("utf-8")
                    if 0 < ydbapron["recipes"][recipe_category][recipe]["ingredients"][category][ingredient].data:
                        ingredients[category][ingredient]["dependencies"].append({"category": recipe_category, "name": recipe})

    return ingredients


def get_ingredient(category: AnyStr, ingredient_name: AnyStr) -> dict:
    ingredient = {}
    ingredient_spec = yottadb.get("^YDBApron", ("ingredients", category, ingredient_name)).decode("utf-8")
    ingredient["amount"] = ingredient_spec.split("|")[0]
    ingredient["unit"] = ingredient_spec.split("|")[1]
    ingredient["cost"] = ingredient_spec.split("|")[2]
    ingredient["manufacturer"] = ingredient_spec.split("|")[3]
    ingredient["vendor"] = ingredient_spec.split("|")[4]

    return ingredient


def get_ingredients(form: RecipeForm = None) -> dict:
    ydbapron = yottadb.Key("^YDBApron")
    ingredients = {}
    ingredient_forms = iter(form.ingredients) if form is not None else None
    for category in ydbapron["ingredients"].subscripts:
        category = category.decode("utf-8")
        ingredients[category] = {}
        for ingredient in ydbapron["ingredients"][category].subscripts:
            ingredient = ingredient.decode("utf-8")
            ingredients[category][ingredient] = get_ingredient(category, ingredient)
            ingredients[category][ingredient]["form"] = next(ingredient_forms)

    return ingredients


@blueprint.route("/list", methods=("GET",))
def list():
    ingredients = list_ingredients()

    return render_template('ingredients.html', ingredients=ingredients)


@blueprint.route("/add", methods=("GET", "POST"))
def add():
    form = IngredientForm(request.form)
    num_ingredients = total_ingredients()
    entries_available = num_ingredients < MAX_INGREDIENTS
    if request.method == "POST" and form.validate():
        if entries_available:
            save_ingredient(form)
            return redirect("/ingredients/list")
        else:
            return render_template('add_ingredient.html', ingredient_form=form, operation="Add", entries_available=entries_available, num_ingredients=num_ingredients)
    else:
        return render_template('add_ingredient.html', ingredient_form=form, operation="Add", entries_available=entries_available, num_ingredients=num_ingredients)


@blueprint.route("/edit/<category>/<ingredient_name>", methods=("GET", "POST"))
def edit(category: AnyStr, ingredient_name: AnyStr):
    form = IngredientForm(request.form)
    if request.method == "POST" and form.validate():
        delete_ingredient(category, ingredient_name)
        save_ingredient(form)
        return redirect("/ingredients/list")
    else:
        ingredient = get_ingredient(category, ingredient_name)
        form.ingredient_name.data = ingredient_name
        form.ingredient_category.data = category
        form.ingredient_amount.data = float(ingredient["amount"])
        form.ingredient_unit.data = ingredient["unit"]
        form.ingredient_price.data = float(ingredient["cost"])
        form.ingredient_manufacturer.data = ingredient["manufacturer"]
        form.ingredient_vendor.data = ingredient["vendor"]

        return render_template('add_ingredient.html', ingredient_form=form, operation="Edit", entries_available=True)


@blueprint.route("/delete", methods=("POST",))
def delete():
    form = request.form
    delete_ingredient(form["deleteCategory"], form["deleteIngredient"])
    return redirect("/ingredients/list")
