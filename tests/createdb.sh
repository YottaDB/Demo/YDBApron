#!/bin/bash
#################################################################
#                                                               #
# Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################

yottadb -run ^GDE <<GDE
change -r DEFAULT -key_size=1019 -record_size=100000
change -segment DEFAULT -file_name=$1
exit
GDE

mupip create
ydb_chset=M mupip load $2
